//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
float input()
{  
    float dim;
    printf("Enter the dimensions of tromboloid \n");
    scanf("%f",&dim);
    return (dim);
}

float tromboloid_volume(float h, float b, float d)
{
    float volume;
    volume= (1/(3*b))*((h*d)+d);
    return volume;
}

void output()
{
    float volume;
    printf("The volume of the tromboloid is %f \n", volume);
}

float main()
{
    float x,y,z,volume;
    x=input();
    y=input();
    z=input();
    volume= tromboloid_volume(x,y,z);
    output(volume);
    return 0;
}

