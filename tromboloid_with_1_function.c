//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>
int main()
{
    float h,b,d,V;
    printf("Enter height,base,characteristic dimensions:");
    scanf("%f %f %f",&h,&b,&d);
    V=((1.0/3.0)*((h*d)+d)/b);
    printf("volume=%f",V);
    return 0;
}